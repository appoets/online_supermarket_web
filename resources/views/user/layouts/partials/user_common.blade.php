<section class="profile-baner">
	<div class="container">
		<div class="row">
			<div class="col-6 mx-auto pt-5">
				<div class="avatar-upload">
                   <div class="avatar-edit">
		                <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
		                <label for="imageUpload"></label>
            		</div>
		            <div class="avatar-preview">
		                <div id="imagePreview" style="background-image: url(storage/{{@Auth::user()->avatar}});">
		                </div>
		            </div>
		        </div>
		        <div class="profile-title text-center">
	            	<h4>{{@Auth::user()->name}}</h4>
	            	<p>{{@Auth::user()->phone}}</p>
	            </div>
			</div>
		</div>
	</div>
</section>	





{{-- <div class="profile-head row m-0">
    <div class="col-md-8 col-sm-8 col-xs-12">
        <h4 class="profile-head-tit">{{@Auth::user()->name}}</h4>
        <p class="profile-head-txt">
            <span>{{@Auth::user()->phone}}</span>
            <span class="dot">.</span>
            <span>{{@Auth::user()->email}}</span>
        </p>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12 text-right">
        <a href="#" class="edit-profile" onclick="$('#edit-profile-sidebar').asidebar('open')">@lang('user.create.edit_profile')</a>
    </div>
</div> --}}
