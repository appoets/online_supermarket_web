<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Transporter;
use Exception;
use Log;
use Setting;
use App\Notification;
use App\Shop;

use Edujugon\PushNotification\PushNotification;


/*use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;
use App\Notifications\WebPush;*/
class SendPushNotification extends Controller
{
	

    /**
     * Money added to user wallet.
     *
     * @return void
     */
    public function WalletMoney($user_id, $money){

        Log::info($user_id);

        return $this->sendPushToUser($user_id, $money.' '.trans('form.push.added_money_to_wallet'));
    }

    /**
     * Money charged from user wallet.
     *
     * @return void
     */
    public function ChargedWalletMoney($user_id, $money){

        return $this->sendPushToUser($user_id, $money.' '.trans('form.push.charged_from_wallet'));
    }

    /**
     * Sending Push to a user Device.
     *
     * @return void
     */
        public function sendPushToUser($user_id, $push_message,$page = null){
        try{           
            $user = User::findOrFail($user_id);

            if($user->device_token != ""){          


                if($user->device_type == 'ios'){
                     if(env('IOS_USER_ENV','development')=='development'){
                        $crt_user_path=app_path().'/apns/user/live_user.pem';
                        $dry_run = false;
                    }
                    else{
                        $crt_user_path=app_path().'/apns/user/live_user.pem';
                        $dry_run = false;
                    }
                    \Log::info('push send to '.$user->id);
                    $push = new PushNotification('apn');

                    $push->setConfig([
                        'certificate' => $crt_user_path,
                        'passPhrase' =>'apple',
                        'dry_run' => $dry_run
                    ]);

                    $send=  $push->setMessage([
                        'aps' => [
                            'alert' => [
                                'body' => $push_message
                            ],
                            'sound' => 'default',
                            'badge' => 1

                        ],
                        'extraPayLoad' => [
                            'custom' => $page
            
                        ]
                    ])->setDevicesToken($user->device_token)->send();
                    \Log::info('sent');
                    
                    return $send;

                }elseif($user->device_type == 'android'){

                   $push = new PushNotification('fcm');
                   $send=  $push->setMessage(['message'=>$push_message,'custom'=>$page])                   
                        ->setDevicesToken($user->device_token)->send();
                    
                   return $send;
                }
            }

        } catch(Exception $e){
            return $e;
        }

    }

     /**
     * Sending Push to a user Device.
     *
     * @return void
     */
    public function sendPushToShop($user_id, $push_message,$page = null){

        try{

            $user = Shop::findOrFail($user_id);
              
            if($user->device_token != ""){

                if($user->device_type == 'ios'){
                     if(env('IOS_SHOP_ENV')=='development'){
                        $crt_shop_path=app_path().'/apns/shop/live_shop.pem';
                        $dry_run = false;
                    }
                    else{
                        $crt_shop_path=app_path().'/apns/shop/live_shop.pem';
                        $dry_run = false;
                    }
                    
                   $push = new PushNotification('apn');

                    $push->setConfig([
                            'certificate' => $crt_shop_path,
                            'passPhrase' => 'apple',
                            'dry_run' => $dry_run
                        ]);

                   $send=  $push->setMessage([
                            'aps' => [
                                'alert' => [
                                    'body' => $push_message
                                ],
                                'sound' => 'default',
                                'badge' => 1

                            ],
                            'extraPayLoad' => [
                                'custom' => $page
                
                            ]
                        ])
                        ->setDevicesToken($user->device_token)->send();
                        \Log::info('sent');
                    
                    return $send;

                }elseif($user->device_type == 'android'){

                   $push = new PushNotification('fcm');
                   $send=  $push->setMessage(['message'=>$push_message,'custom'=>$page])
                        ->setDevicesToken($user->device_token)->send();
                    
                    return $send;
                       


                }
            }

        } catch(Exception $e){
            return $e;
        }

    }

    /**
     * Sending Push to a user Device.
     *
     * @return void
     */
    public function sendPushToProvider($provider_id, $push_message,$page = null){

        try{

            $provider = Transporter::findOrFail($provider_id);
            if($provider->device_token != ""){


                if($provider->device_type == 'ios'){

                    if(env('IOS_PROVIDER_ENV')=='development'){
                        $crt_user_path=app_path().'/apns/user/live_provider.pem';
                        $crt_provider_path=app_path().'/apns/provider/live_provider.pem';
                        $dry_run = false;
                    }
                    else{
                        $crt_user_path=app_path().'/apns/user/live_provider.pem';
                        $crt_provider_path=app_path().'/apns/provider/live_provider.pem';
                        $dry_run = false;
                    }

                   $push = new PushNotification('apn');
                   $push->setConfig([
                            'certificate' => $crt_provider_path,
                            'passPhrase' => 'apple',
                            'dry_run' => $dry_run
                        ]);
                   $send=  $push->setMessage([
                            'aps' => [
                                'alert' => [
                                    'body' => $push_message
                                ],
                                'sound' => 'default',
                                'badge' => 1

                            ],
                            'extraPayLoad' => [
                                'custom' => $page
                            ]
                        ])
                        ->setDevicesToken($provider->device_token)->send();
                
                    
                    return $send;

                }elseif($provider->device_type == 'android'){
                    
                   $push = new PushNotification('fcm');
                   $send=  $push->setMessage(['message'=>$push_message,'custom'=>$page])
                        ->setDevicesToken($provider->device_token)->send();
                    
                    return $send;         

                }
            }

        } catch(Exception $e){
            return $e;
        }

    }

}
