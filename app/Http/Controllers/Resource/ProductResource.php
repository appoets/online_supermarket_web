<?php

namespace App\Http\Controllers\Resource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Image;
use Route;
use Exception;
use App\Addon;
use App\Product;
use App\Category;
use App\ProductImage;
use App\ProductPrice;
use Setting;
use App\AddonProduct;
use Log;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;


use PHPExcel_Worksheet_Drawing;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class ProductResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('demo', ['only' => ['store', 'update']]);
    }
    
    public function index(Request $request, $category = null)
    {
        try {
            $Category = Category::productList($request->shop)->findOrFail($category);
            return view(Route::currentRouteName(), compact('Category'));
        } catch (ModelNotFoundException $e) {
            $Products = Product::list($request->shop);
            if($request->ajax()){
                return $Products;
            }
            return view(Route::currentRouteName(), compact('Products'));
        }
    }

    public function product_bulk(Request $request)
    {
        return view('admin.products.create_bulk');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   
        try{

            if(Setting::get('PRODUCT_ADDONS',1)){
                $Addons = Addon::where('shop_id',$request->shop)->get();
            }else{
                $Addons = [];
            }
            if(Setting::get('SUB_CATEGORY',1)){
                $Categories = Category::where('shop_id',$request->shop)->where('parent_id','=','0')->get();
            }else{
                $Categories = Category::where('shop_id',$request->shop)->get(); 
            }
             return view(Route::currentRouteName(),compact('Addons','Categories'));
            
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', trans('form.whoops'));
        }catch(Exception $e){
            return back()->with('flash_error', trans('form.whoops'));
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'name' => 'required|string|max:255',
                'description' => 'required|string|max:255',
                'image' => 'required',
                'category' => 'required',
               // 'food_type' => 'required',
                'price' => 'required',
                'product_position' => 'required'
            ]);
        try {

            if($request->has('featured')) {
                $request->featured_position = $request->featured_position?$request->featured_position:1;
            }
            if($request->has('discount')) {

                if($request->discount_type=='percentage'){
                    $orignal_price = number_format(($request->price-($request->price*($request->discount/100))),2,'.','');
                }else{
                    $orignal_price = number_format(($request->price-$request->discount),2,'.','');
                }

            }else{
                $orignal_price = number_format($request->price,2,'.','');;
            }

            $Product = Product::create([
                    'shop_id' => $request->shop,
                    'name' => $request->name,
                    'description' => $request->description,
                    'featured' => $request->featured?$request->featured_position:0,
                   // 'food_type' => $request->food_type,
                    'parent_id' => $request->parent_id,
                    'position' => $request->product_position,
                    'ingredients'=>$request->ingredients,
                    'directions'=>$request->directions,
                    'warnings'=>$request->warnings,
                    'product_info_img' => isset($request->product_info_img) ? asset('storage/'.$request->product_info_img->store('products')) : '',


            ]);
            
            $Product->categories()->attach($request->category);
            
            foreach($request->image as $key =>$img){
                ProductImage::create([
                    'product_id' => $Product->id,
                    'url' => asset('storage/'.$img->store('products')),
                    'position' => 0,
                ]);
            }


            if($request->hasFile('featured_image')) {
               
                ProductImage::create([
                    'product_id' => $Product->id,
                    'url' => asset('storage/'.$request->featured_image->store('products')),
                    'position' => 1,
                ]);
            }

            ProductPrice::create([
                    'product_id' => $Product->id,
                    'price' => $request->price,
                    'currency' => Setting::get('currency'),
                    'discount' => $request->discount,
                    'discount_type' => $request->discount_type,
                    'orignal_price' => $orignal_price
                ]);
            if($request->has('addons')){
                $addons = $request->addons;
                $addons_price = $request->addons_price;
                foreach($addons as $key=>$val){
                    AddonProduct::create([
                        'addon_id' => $val,
                        'product_id' => $Product->id,
                        'price' => $addons_price[$val]
                        ]);
                    //$Product->addons()->attach($val,['price' => $addons_price[$val]]);
                    //$Shop->cuisines()->attach($cuisine);
                }
            }

            // return redirect()->route('admin.products.show', $request->category)->with('flash_success', 'Product Added');
            return back()->with('flash_success', 'Product Added');
        } catch (Exception $e) {
            // return redirect()->route('admin.products.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id,$category = NULL)
    {
        try {
            $Product = Product::with('prices','images','addons','cart')->findOrFail($id);
            if($request->ajax()){
                return $Product;
            }
            return view(Route::currentRouteName(), compact('Product'));
        } catch (Exception $e) {
            if($request->ajax()) {
                return response()->json(['message' => 'Not Found!'], 404);
            }
            // return redirect()->route('admin.products.index', $category)->with('flash_error', 'Not Found!');            
            return back()->with('flash_error', 'Not Found!');            
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        try {
            $Product = Product::with('addons')->findOrFail($id);
            //dd($Product);
            if(Setting::get('PRODUCT_ADDONS',1)){
                $Addons = Addon::where('shop_id',$request->shop)->get();
            }else{
                $Addons = [];
            }
            if(Setting::get('SUB_CATEGORY',1)){
               $Categories = Category::with('subcategories')->where('shop_id',$request->shop)->where('parent_id','=','0')->get();
            }else{
                $Categories = Category::where('shop_id',$request->shop)->get(); 
            }
            
            //dd($Categories);
            return view(Route::currentRouteName(), compact('Product','Addons','Categories'));
        } catch (ModelNotFoundException $e) {
            // return redirect()->route('admin.products.index')->with('flash_error', 'Product not found!');
            return back()->with('flash_error', 'Product not found!');
        } catch (Exception $e) {
            // return redirect()->route('admin.products.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                'name' => 'required|string|max:255',
                'description' => 'required|string|max:255',
                //'position' => 'required|integer',
                //'image' => 'image',
                'category' => 'required',
                'price' => 'required',
                'product_position' => 'required'
            ]);

        try {
            $Product = Product::findOrFail($id);

            $Update = $request->all();

            if($request->has('featured')) {
                $Update['featured'] = $Update['featured_position']?:1;
            }else{
                $Update['featured'] = 0;
            }

            if($request->has('product_position')) {
                $Update['position'] = $Update['product_position']?:'';
            }
            if($request->has('addon_status')) {
                $Update['addon_status'] = 1;
            }else{
                $Update['addon_status'] = 0;
            }

            if($request->hasFile('product_info_img')){
               $Update['product_info_img'] = asset('storage/'.$request->product_info_img->store('products'));
            }

            $Product->update($Update);
            if($request->has('discount')) {

                if($request->discount_type=='percentage'){
                    $orignal_price = $request->price-($request->price*($request->discount/100));
                }else{
                    $orignal_price = $request->price-$request->discount;
                }

            }else{
                $orignal_price = $request->price;
            }
                $Product->prices->update([
                    'price' => $request->price,
                    'currency' => Setting::get('currency'),
                    'discount' => $request->discount,
                    'discount_type' => $request->discount_type,
                    'orignal_price' => $orignal_price
                ]);       

            $Product->categories()->detach();
            if($request->has('category')) {
                $Product->categories()->attach($request->category);
            }

            if($request->hasFile('image')) {
                //if($Product->images->isEmpty()) {
                foreach($request->image as $key =>$img){
                    //$thumb_url = Image::make($img)->resize(50, 50)->save(storage('products/thumb/' . $img->name));
                    ProductImage::create([
                            'product_id' => $Product->id,
                            'url' => asset('storage/'.$img->store('products')),
                            //'thumb_url' => asset($thumb_url),
                            'position' => 0,
                        ]);
                }
                /*} else {
                    $Product->images[0]->update(['url' => asset('storage/'.$request->image->store('products'))]);
                }*/
            }

            if($request->hasFile('featured_image')) {
                //if($Product->images->isEmpty()) {
                if($product_image = ProductImage::where('product_id',$Product->id)->where('position',1)->first()){
                    $product_image->update(['url' => asset('storage/'.$request->featured_image->store('products'))]);
                }else{
                    ProductImage::create([
                            'product_id' => $Product->id,
                            'url' => asset('storage/'.$request->featured_image->store('products')),
                            'position' => 1,
                    ]);
                }
                /*} else {
                    $Product->images[0]->update(['url' => asset('storage/'.$request->image->store('products'))]);
                }*/
            }

            if($request->has('addons')){
                
                $addons = $request->addons;
                $addons_price = $request->addons_price;
               // $Product->addons()->detach();

                $add_prod_all =AddonProduct::where('product_id',$Product->id)->pluck('addon_id','addon_id')->toArray();
                $remove_addons =array_diff($add_prod_all,$addons);
                if(count($remove_addons)>0){
                    sort($remove_addons);
                    AddonProduct::whereIn('addon_id',$remove_addons)->delete();
                }
                //print_r($remove_addons); exit;
                foreach($addons as $key=>$val){
                    $product_addon = AddonProduct::where('product_id',$Product->id)->where('addon_id',$val)->first();
                    if(count($product_addon)>0){
                        $product_addon->price = $addons_price[$val];
                        $product_addon->save();
                    }else{
                        AddonProduct::create([
                            'addon_id' => $val,
                            'product_id' => $Product->id,
                            'price' => $addons_price[$val]
                        ]);
                    }
                    //$Shop->cuisines()->attach($cuisine);
                }
            }else{
                $add_prod_all =AddonProduct::where('product_id',$Product->id)->pluck('addon_id','addon_id')->toArray();
                 if(count($add_prod_all)>0){
                    AddonProduct::whereIn('addon_id',$add_prod_all)->delete();
                }
            }


            // return redirect()->route('admin.products.index')->with('flash_success', 'Product updated!');
            return back()->with('flash_success', 'Product updated!');
        } catch (ModelNotFoundException $e) {
            // return redirect()->route('admin.products.index')->with('flash_error', 'Product not found!');
            return back()->with('flash_error', 'Product not found!');
        } catch (Exception $e) {
            dd($e);
            // return redirect()->route('admin.products.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Product = Product::findOrFail($id);
            //$addon_prod = AddonProduct::where('product_id',$id)->delete();
            // Need to delete variants or have them re-assigned
            $Product->delete();
            // return redirect()->route('admin.products.index')->with('flash_success', 'Product deleted!');
            \App\UserCart::where('product_id',$id)->whereNull('deleted_at')->delete();
            return back()->with('flash_success', 'Product deleted!');
        } catch (ModelNotFoundException $e) {
            // return redirect()->route('admin.products.index')->with('flash_error', 'Product not found!');
            return back()->with('flash_error', 'Product not found!');
        } catch (Exception $e) {
            // return redirect()->route('admin.products.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function imagedestroy($id)
    {
         try {
            $Product = ProductImage::findOrFail($id);
            $Product->delete();
            return back()->with('flash_success', 'Product Image deleted!');
        } catch (ModelNotFoundException $e) {
            // return redirect()->route('admin.products.index')->with('flash_error', 'Product not found!');
            return back()->with('flash_error', 'Product not found!');
        } catch (Exception $e) {
            // return redirect()->route('admin.products.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    public function product_csv_9(Request $request)
    {
        if ($request->hasFile('product_file') ){

            $file = $request->file('product_file');
      
            // File Details 
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();
      
            // Valid File Extensions
            $valid_extension = array("csv");
      
            // 2MB in Bytes
            $maxFileSize = 2097152; 
      
            // Check file extension
            if(in_array(strtolower($extension),$valid_extension)){
      
              // Check file size
              if($fileSize <= $maxFileSize){
      
                // File upload location
                $location = 'uploads';
      
                // Upload file
                $file->move($location,$filename);
      
                // Import CSV to Database
                $filepath = public_path($location."/".$filename);
      
                // Reading file
                $file = fopen($filepath,"r");
      
                $importData_arr = array();
                $i = 0;
      
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                   $num = count($filedata );
                   
                   // Skip first row (Remove below comment if you want to skip the first row)
                   if($i == 0){
                      $i++;
                      continue; 
                   }
                   for ($c=0; $c < $num; $c++) {
                      $importData_arr[$i][] = $filedata [$c];
                   }
                   $i++;
                }
                fclose($file);
                
                $duplicate = array();
                $result = array();
                foreach ($importData_arr as $key => $value){
                    if(!in_array($value, $result)){
                        $result[$key]=$value;
                    }else{
                    //     $duplicate[$key] =$value;
                    //    // print_r($duplicate); 

                    //     // Import CSV to Database
                    //     $filepath = public_path("duplicate/"."dupli.csv");

                    //     $file = fopen($filepath,"w");
                    //     fputcsv($file, array('name', 'price', 'description', 'url'));

                    //     foreach ($duplicate as $line) {
                    //         fputcsv($file, $line, ',');
                    //     }
                    }
                }
                // Insert to MySQL database
                foreach($result as $importData){

                    $insertData = array(
                    "shop_id"=>1,
                     "name"=>$importData[0],
                     "description"=>$importData[1],
                     "brand"=>$importData[2],
                     "nutrition"=>$importData[3],
                      "directions"=>$importData[4],
                     "out_of_stock"=>$importData[5],
                      "status"=>$importData[6],
                      "ingredients"=>$importData[7],
                      "warnings"=>$importData[8],
                     "product_info_img"=>$importData[9]
                 );
                  Product::insert($insertData);
      
                }
      
                return back()->with('flash_success','Import Successful.');
              }else{
                return back()->with('flash_error','File too large. File must be less than 2MB.');
              }
      
            }else{
               return back()->with('flash_success','Invalid File Extension.');
            }
      
          }
      
          // Redirect to index
                return back()->with('flash_error','Invalid file.');
        
    }
    public function product_csv(Request $request)
    {

        if(Input::hasFile('product_file')){
            $path = Input::file('product_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                  //  dd($value);

                  $product_image = $value->image;

                  //  $image_product = array();
                    $image_product = explode( ";", $product_image );
                    //  dd($image_product);
                    $product_images_for = array();

                    foreach ($image_product as $key => $pro_img) {
                       
                        if ($pro_img) 
                        {
                            $extension = pathinfo($pro_img, PATHINFO_EXTENSION);

                                $product_img = \Image::make(file_get_contents($pro_img))->widen(860, function ($constraint) {
                                $constraint->upsize();
                                });
                                $dir = 'products/';
                                $path = uniqid().'.jpg';
                                $product_img->encode();
                                Storage::put($dir.$path, (string)$product_img);

                                $destination = '/storage/products/';
                                $product_images_for[] = $destination.$path;
                        }else{
                            $product_images_for = null;
                        }
                    }
        // dd($product_images_for);

                   // print_r($product_images_for);

                        $file = $value->product_info_img;
                        if ($file) 
                        {
                            $ext = pathinfo($file, PATHINFO_EXTENSION);

                               $main_image = \Image::make(file_get_contents($file))->widen(860, function ($constraint) {
                                 $constraint->upsize();
                               });
                               $dir = 'products/';
                               $path = uniqid().'.jpg';
                               $main_image->encode();
                               Storage::put($dir.$path, (string)$main_image);

                               $destination = env('APP_URL').'/storage/products/';
                               $product_image = $destination.$path;
                        }else{
                            $product_image = null;
                        }

                        $featured_product_image = $value->featured_image;
                        if ($featured_product_image) 
                        {
                            $extension = pathinfo($featured_product_image, PATHINFO_EXTENSION);

                               $product_image_featured = \Image::make(file_get_contents($featured_product_image))->widen(860, function ($constraint) {
                                 $constraint->upsize();
                               });
                               $dir = 'products/';
                               $path = uniqid().'.jpg';
                               $product_image_featured->encode();
                               Storage::put($dir.$path, (string)$product_image_featured);

                               $destination = env('APP_URL').'/storage/products/';
                               $featured_image = $destination.$path;
                        }else{
                            $featured_image = null;
                        }

                

                    $product = new Product();
                    
                    $product->shop_id = $request->shop;
                    $product->name= $value->name;
                    $product->description=$value->description ?: '';
                    $product->brand=$value->brand?: '';
                    $product->nutrition=$value->nutrition?: '';
                    $product->directions=$value->directions?:'';
                    $product->out_of_stock=$value->out_of_stock?:'YES';
                    $product->status=$value->status?:'enabled';
                    $product->ingredients=$value->ingredients?:'';
                    $product->warnings=$value->warnings?:'';
                    $product->product_info_img=$product_image?:'';
                    $product->featured = $value->featured?:'0';
                     $product->parent_id =$value->parent_id?:'0';
                     $product->position = $value->product_position?:'0';
                    $product->save();
                    

                    $category_id = Category::where('name',$value->category)->first();

                    $category_pro = $category_id->id;

                    $product->categories()->sync($category_pro);

                    if (is_array($product_images_for) || is_object($product_images_for)){

                        foreach($product_images_for as $arr_img){
                            // dd($arr_img);

                            ProductImage::create([
                                'product_id' => $product->id,
                                'url' => $arr_img,
                                'position' => 0,
                            ]);
                        }
                    }

                    ProductImage::create([
                        'product_id' => $product->id,
                        'url' => $featured_image,
                        'position' => 1,
                    ]);

                    if ($value->discount){
                        if($value->discount_type == "percentage") {

                            $orignal_price = number_format(($value->price-($value->price*($value->discount/100))),2,'.','');
                        }else{
                            $orignal_price = number_format(($request->price-$request->discount),2,'.','');
                        }
        
                    }else{
                        $orignal_price = number_format($value->price,2,'.','');;
                    }

                    ProductPrice::create([
                        'product_id' => $product->id,
                        'price' => $value->price,
                        'currency' => Setting::get('currency'),
                        'discount' => $value->discount,
                        'discount_type' => $value->discount_type,
                        'orignal_price' => $orignal_price
                    ]);       
    
                  
                   
            }
            return back()->with('flash_success','Import Successful.');

        }
        return back()->with('flash_error','Import failed.');
    }
}
   

}