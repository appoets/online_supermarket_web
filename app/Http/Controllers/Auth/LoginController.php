<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Session;
use Redirect;
use Illuminate\Notifications\Messages\MailMessage;
use Validator;
use Twilio;
use Hash;
use URL;
use Log;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->redirectTo = URL::previous();
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('user.auth.login');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */

    public function username()
    {       
        return 'phone';  
    }   

    public function login(Request $request){

        Log::info($request->all());
        $this->validate($request, [
            'phone' => 'required',
            'password' => 'required',

            ]);
        if (\Auth::attempt([
            'phone' => $request->login_code . $request->phone,
            'password' => $request->password])
        ){
            return redirect('/');
        }
        return redirect('/')->with('flash_error', 'Invalid Credential!!');
    }
}
