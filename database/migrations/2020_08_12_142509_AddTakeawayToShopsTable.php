<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTakeawayToShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shops', function (Blueprint $table) {
            $table->integer('takeaway')->default(0);   
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->integer('takeaway')->default(0)->after('transporter_vehicle_id');   
        });
        Schema::table('order_invoices', function (Blueprint $table) {
            $table->integer('takeaway')->default(0)->after('discount');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shops', function (Blueprint $table) {
            $table->dropColumn('takeaway');  
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('takeaway');  
        });
        Schema::table('order_invoices', function (Blueprint $table) {
            $table->dropColumn('takeaway');   
        });

    }
}
