<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEzgroceryGstHstToOrderInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_invoices', function (Blueprint $table) {

            $table->double('shop_gross',10,2)->default(0)->after('paid');      
            $table->double('ezgrocery_gross_commision',10,2)->default(0)->after('shop_gross');      
            $table->double('shop_gst_hst',10,2)->default(0)->after('ezgrocery_gross_commision');      
            $table->double('ezgrocery_gst_hst',10,2)->default(0)->after('shop_gst_hst');      
            $table->double('tips',10,2)->default(0)->after('ezgrocery_gst_hst');      
            $table->double('delivery_commission',10,2)->default(0)->after('tips');      
            $table->double('delivery_gross',10,2)->default(0)->after('delivery_commission');      
            $table->double('delivery_gst_hst',10,2)->default(0)->after('delivery_gross');      
            $table->double('ezgrocery_delivery_gst_hst',10,2)->default(0)->after('delivery_gst_hst');      

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_invoices', function (Blueprint $table) {
            //
        });
    }
}
